#!/bin/bash

transcode_exec()
{
    check_dry_run=$@

    #TODO videoSRC and tmpvideo should be local arguments
    $check_dry_run  -nostdin -loglevel warning -i $videoSRC -y -acodec aac -vcodec libx264 -r 24 -filter:v "scale='w=trunc(oh*a/2)*2':'h=$TO_RES'" -f mp4 -level 3.1 -b_strategy 1 -bf 16 -pix_fmt yuv420p -map_metadata -1 -movflags faststart -maxrate 460000 -bufsize 920000 -g 48  $tmpvideo 
}

# copy to transocde server then get result back
# network consuming, better through a LAN.
transcode_remote()
{
    originalSRC=$videoSRC
    originalTMP=$tmpvideo
    videoSRC=$transcode_tmp/$(basename originalSRC)
    scp $originalSRC $transcode_server:$videoSRC
    tmpvideo=$transcode_tmp/${videoUUID}-$TO_RES.mp4
    # use global ffmpeg
    remote_command=$(transcode_exec echo ffmpeg);ssh $transcode_server $remote_command
    scp $transcode_server:$tmpvideo $originalTMP
    tmpvideo=$originalTMP
}

fake_transcode()
{
    videoUUID=$1
    videoSRC=$2
    videoExpectedDST=$3
    TO_RES=$4
    tmpvideo=$peertube_tmp/${videoUUID}-$TO_RES.mp4

    transcode_exec echo sudo -u peertube $FFMPEG
}

peertubehack_transcode()
{
    if [[ -n $dry_run ]]
    then
	fake_transcode $@
	return
    fi

    videoUUID=$1
    videoSRC=$2
    videoExpectedDST=$3
    TO_RES=$4
    tmpvideo=$peertube_tmp/${videoUUID}-$TO_RES.mp4
    
    if [[ -f $videoSRC ]]
    then
	if [[ -f  $tmpvideo ]]
	then
	    # might be legit if previous import run failed... TODO
	    echo "[ERROR] temporary $tmpvideo already exists... please proceed with cleaning or waiting concurrent process" >&2
	else
	    echo "[INFO] processing $videoSRC. generate $tmpvideo using ffmpeg"
	    if [[ -z $transcode_server ]]
	    then
		transcode_exec sudo -u peertube $FFMPEG 2>${tmpvideo}.log
	    else
		transcode_remote
	    fi
	    if [[ -f $tmpvideo ]]
	    then
		pushd $PEERTUBE_CODE_DIR
		videoFile=$tmpvideo
		sudo -u peertube NODE_CONFIG_DIR=$PEERTUBE_ROOT_DIR/config NODE_ENV=production npm run create-import-video-file-job -- -v $videoUUID -i $videoFile
		sleep 2s
		if [[ -e $videoExpectedDST ]]
		then
		    rm $tmpvideo
		else
		    echo "[ERROR] Expected to find $videoExpectedDST after create-import-video-file-job but no file found. keeping $tmpvideo" >&2
		fi
		popd
	    fi
	fi
    fi
    return 0
}

# read yaml config and echo storage.videos content
extract_storage()
{
    while read line 
    do
	if [[ $line =~ ^storage ]]
	then
	    in_storage=$line
	elif [[ -z $line ]]
	then
	    in_storage=
	elif [[ -n $in_storage ]] && [[ $line =~ ^videos:\ \'(.*)\' ]]
	then
	    peertube_storage=${BASH_REMATCH[1]}
	    echo "$peertube_storage"
	    return
	fi
    done
}


# tribute to JohnLivingston
detect_peertube_code_dir()
{    
    # brainless...
    if [[ -d $PEERTUBE_ROOT_DIR/scripts ]]
    then
	PEERTUBE_CODE_DIR=$PEERTUBE_ROOT_DIR
    elif [[ -d $PEERTUBE_ROOT_DIR/peertube-latest/scripts ]]
    then	
	PEERTUBE_CODE_DIR=$PEERTUBE_ROOT_DIR/peertube-latest
    else
	PEERTUBE_CODE_DIR=$PEERTUBE_ROOT_DIR/peertube-latest
	echo "[ERROR] no scripts dir found within $PEERTUBE_CODE_DIR" >&2
    fi

}

detect_peertube_storage_dir()
{
    if [[ -d /home/yunohost.app/peertube/storage ]]
    then
	# yunohost case
	peertube_storage=/home/yunohost.app/peertube/storage
	peertube_video=${peertube_storage}/videos
    else
	# quick parse production.yaml
	peertube_config=$PEERTUBE_ROOT_DIR/config/production.yaml
	if [[ -f $peertube_config ]]
	then	    
	    peertube_video=$(cat $peertube_config | extract_storage)
	else
	    echo "[ERROR] config $peertube_config  not found" <&2
	fi
    fi
}

# main()

PEERTUBE_ROOT_DIR=/var/www/peertube/

peertube_tmp=$(pwd)/tmp

echo "[WARNING] $peertube_tmp should be writable by peertube user"

# if transcode_server is set then remote encoding is used
# transcode_server=user@transcodeserver
transcode_tmp=transcode_tmp

while [[ $# > 0 ]]
do
    case $1 in
	peertube_root_dir=*)
	    PEERTUBE_ROOT_DIR=${1/peertube_root_dir=/}
	    ;;
	transcode_server=*)
	    transcode_server=${1/transcode_server=/}
	    ;;
	transcode_tmp=*)
	    transcode_tmp=${1/transcode_tmp=/}
	    ;;
	peertube_tmp=*)
	    peertube_tmp=${1/peertube_tmp=/}
	    ;;
	dry_run)
	    dry_run=$1
	    ;;
	*)
	    if [[ -z $infile ]]
	    then
		infile=$1
	    else
		echo "[ERROR] only infile expected '$1' extra parameter" >&2
		exit 1
	    fi
	    ;;
    esac
    shift
done

detect_peertube_code_dir

# Not tested with other resolutions than 1080 -> 360
#FROM_RES=1080
FROM_RES=540
TO_RES=360

echo "goal is to add a new $TO_RES p resolution to an existing video in $FROM_RES p"

echo " expecting a file containing list of url belonging to a plist one url by line, search for videoId="

if [[ ! -e $infile ]]
then
    echo "[ERROR] Missing infile=$infile" >&2
    exit 1
fi

FFMPEG=$(which ffmpeg)

if [[ ! -x $FFMPEG ]]
then
    if [[ -z $dry_run ]]
    then
	echo "[FATAL] no $FFMPEG executable binary. Exiting" >&2
    else
	echo "[WARNING] no $FFMPEG executable binary" >&2
    fi
fi

detect_peertube_storage_dir

if [[ -z $peertube_video ]]
then
    echo "[FATAL] no peertube storage video directory found" >&2
    exit 1
fi


cat $infile | {
    while read line
    do
	if [[ $line =~ videoId=(.*) ]]
	then
	    echo $line
	    videoUUID=${BASH_REMATCH[1]}
	    videoFROM_RES=${peertube_video}/${videoUUID}-$FROM_RES.mp4
	    videoTO_RES=${peertube_video}/${videoUUID}-$TO_RES.mp4
	    if [[ -f $videoFROM_RES ]]
	    then
		if [[ -e $videoTO_RES ]]
		then
		    echo "$videoTO_RES already exist, skipping"
		else
		    peertubehack_transcode "$videoUUID" "$videoFROM_RES" "$videoTO_RES" $TO_RES
		fi
	    else
		if [[ -n $dry_run ]]
		then
		    fake_transcode "$videoUUID" "$videoFROM_RES" "$videoTO_RES" $TO_RES
		else
		    echo "[ERROR] no $videoFROM_RES to transcode to ${TO_RES}p found. Skipping" >&2
		fi		
	    fi					 
	fi
    done
}
	
